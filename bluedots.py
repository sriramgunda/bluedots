import os, sys

path = "./sample-project"
fc = 0
#dirs = os.listdir(path)
langdict = {"cs":"csharp", "java": "java", "py":"python", "html":"html",
         "css":"css", "js":"javascript", "jpg":"images", "png":"images",
            "php":"php"}
langlist = []
delim = "."
for root, dirs, files in os.walk(path):
    for file in files:
        #print(os.path.join(root, file))
        lang = os.path.join(root, file).split(delim)[-1]
        if(lang in langdict.keys()):
            #print(langdict.get(lang) + " found")
            fc += 1
            if((langdict.get(lang)) not in langlist):
                langlist.append(langdict.get(lang))
print(str(fc) + " files found")
print("The given project contains the following languages: ")
for i in langlist:
    print(i)
